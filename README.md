#coding convention

1. Variable Name:
- The first letter is the data type of the variable
    + int: n
    + bool: b
    + string: s
    + char: c
    + float: f
    + long: l
    + void: v
    + unsigned: u
    + singed n
    + global variable: g
    + static variable: s
    + pointer: p
- Variable names capitalize the first letter of each word.
Ex: g_psNumberCount

2. Function Name:
- Capitalize the first letter of words in the function, except the first letter.
Ex: functionName()

3. Class Name:
- Capitalize the first letter of words of the class name.
Ex: ClassName

4. Define Name:
- Capitalize the define name, between words connected by a '_' sign.
Ex: #define THIS_IS_DEFINE
